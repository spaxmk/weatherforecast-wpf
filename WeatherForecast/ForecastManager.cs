﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;

namespace WeatherForecast
{
    public class ForecastManager : INotifyPropertyChanged
    {
        private const string cityListFilePath = @"../../res/city_list.json";
        private const string iconRes = @"./res/img/icons/";
        private const string historyCitiesPath = @"../../res/history.json";
        private const string favoritesCitiesPath = @"../../res/favorites.json";
        private const string urlGeoIpDb = @"https://geoip-db.com/json";

        private string apiUrl;
        private Thread thread;
        private CityList cityList;
        //public string refreshTime;

        private City selected;

        public IEnumerable<City> Cities { get; }

        public City SelectedCity
        {
            get { return selected; }
            set { selected = value; }
        }

        private int counter = 0;
        
        private Weather weather;

        public Weather Weather
        {
            get { return weather; }
            set
            {
                weather = value;
                OnPropertyChanged("Weather");
            }
        }
        public void resetCounter()
        {
            counter = 0;
        }


        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public ForecastManager()
        {
            FindMyLocation();
            ReadCityList();
            RefreshData();

            Cities = new List<City>();
            foreach (var city in cityList.cities)
            {
                (Cities as List<City>).Add(city);
            }

            thread = new Thread(new ThreadStart(ReadData));
            thread.IsBackground = true;
            thread.Start();
        }

        // Calls API and gets new information. 
        // Deserializes data into Weather property
        public void RefreshData()
        {
            string response = GetDataFromApi();
            Weather = JsonConvert.DeserializeObject<Weather>(response);
            //refreshTime = DateTime.Now.ToLongTimeString();
            FixData();
        }

        private void FixData()
        {
            MyWeather mojObjekat = new MyWeather();
            weather.myList = new List<MyWeather>();

            mojObjekat.date = weather.list[0].dt_txt.Substring(0, 10);
            mojObjekat.min_temp = weather.list[0].main.temp_max;
            mojObjekat.max_temp = weather.list[0].main.temp_max;
            mojObjekat.description = weather.list[0].weather[0].description;
            mojObjekat.id = weather.list[0].weather[0].id;
            mojObjekat.icon = iconRes + Weather.list[0].weather[0].icon + ".png";
            mojObjekat.original_date = weather.list[0].dt_txt.Substring(0, 10);
            mojObjekat.updatedTime = DateTime.Now.ToLongTimeString();

            for (int i = 0; i < weather.list.Count(); i++)
            {
                Weather.list[i].weather[0].icon = iconRes + Weather.list[i].weather[0].icon + ".png";

                if (weather.list[i].dt_txt.Contains(mojObjekat.date))
                {
                    if (weather.list[i].main.temp_min < mojObjekat.min_temp)
                    {
                        mojObjekat.min_temp = weather.list[i].main.temp_min;
                    }

                    if (weather.list[i].main.temp_max > mojObjekat.max_temp)
                    {
                        mojObjekat.max_temp = weather.list[i].main.temp_max;
                    }
                }

                else
                {
                    mojObjekat.date = mojObjekat.date.Substring(8, 2) + "." + mojObjekat.date.Substring(5, 2) + "." + mojObjekat.date.Substring(0, 4) + ".";
                    weather.myList.Add(mojObjekat);
                    mojObjekat = new MyWeather();

                    mojObjekat.original_date = weather.list[i].dt_txt.Substring(0, 10);
                    mojObjekat.date = weather.list[i].dt_txt.Substring(0, 10);
                    mojObjekat.min_temp = weather.list[i].main.temp_min;
                    mojObjekat.max_temp = weather.list[i].main.temp_max;
                    mojObjekat.description = weather.list[i].weather[0].description;
                    mojObjekat.id = weather.list[i].weather[0].id;
                    mojObjekat.icon = weather.list[i].weather[0].icon;
                }
            }

            if (weather.myList.Count() < 5)
            {
                weather.myList.Add(mojObjekat);
            }

            OnPropertyChanged("Weather");
        }

        // Creates new URL for API request based on latitude and longitude of the location
        private void CreateUrl(float latitude, float longitude)
        {
            apiUrl = @"http://api.openweathermap.org/data/2.5/forecast?lat="
                    + latitude + "&lon=" + longitude + "&units=metric&APPID=ba8c996f1de322109b5e38009ce08b63";
        }
        //api NSa
        //apiUrl = @"http://api.openweathermap.org/data/2.5/forecast?lat=45.2517&lon=19.8369&units=metric&APPID=ba8c996f1de322109b5e38009ce08b63";

        private void FindMyLocation()
        {
            disableCertificate();
            string data = new WebClient().DownloadString(urlGeoIpDb);
            IPLocation location = JsonConvert.DeserializeObject<IPLocation>(data);
            CreateUrl(location.latitude, location.longitude);
        }

        static void disableCertificate()
        {
            // Disabling certificate validation can expose you to a man-in-the-middle attack
            // which may allow your encrypted message to be read by an attacker
            // https://stackoverflow.com/a/14907718/740639
            ServicePointManager.ServerCertificateValidationCallback =
                delegate (
                    object s,
                    X509Certificate certificate,
                    X509Chain chain,
                    SslPolicyErrors sslPolicyErrors
                )
                {
                    return true;
                };

        }

        // Reads all available cities from json file and stores it in cityList
        private void ReadCityList()
        {
            using (StreamReader stream = new StreamReader(cityListFilePath))
            {
                string data = stream.ReadToEnd();
                cityList = JsonConvert.DeserializeObject<CityList>(data);
            }
        }

        //Check internet connection
        //private bool CheckConn()
        //{
        //    bool stats = false;
        //    try
        //    {
        //        if (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable() == true)
        //        {
        //            stats = true;
        //        }
        //        else
        //        {
        //            stats = false;
        //        }
        //        return stats;
        //    }
        //    catch (Exception e)
        //    {
        //        MessageBox.Show(e.ToString());
        //    }
        //    return stats;
        //}

        // Reads new data every 10 minutes from the API
        private void ReadData()
        {
            
            while (true)
            {
                Thread.Sleep(10 * 60 * 1000); // Every 10min get new data 
                RefreshData();
            }
            
        }

        // Returns string data representation from API
        private string GetDataFromApi()
        {
            string response = null;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(apiUrl);
            request.AutomaticDecompression = DecompressionMethods.GZip;

            using (HttpWebResponse res = (HttpWebResponse)request.GetResponse())
            {
                using (Stream stream = res.GetResponseStream())
                {
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        response = reader.ReadToEnd();
                    }
                }
            }

            request.Abort();

            return response;

        }
    }
}
