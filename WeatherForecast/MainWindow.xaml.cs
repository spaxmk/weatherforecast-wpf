﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace WeatherForecast
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ForecastManager forecastManager;
        public MainWindow()
        {
            try
            {
                if (CheckConn())
                {
                    InitializeComponent();
                    forecastManager = new ForecastManager();
                    this.DataContext = forecastManager;
                    //lblUpdate.Content += DateTime.Now.ToLongTimeString();
                    DispatcherTimer timer = new DispatcherTimer();
                    timer.Interval = TimeSpan.FromSeconds(1);
                    timer.Tick += timer_Tick;
                    timer.Start();
                    lblDate.Content = DateTime.Now.ToString("dddd, MMM dd yyyy");
                }
                else
                {
                    MessageBoxResult result = MessageBox.Show(this, "Internet connection problem!", "Confirmation", MessageBoxButton.OK, MessageBoxImage.Warning);
                    Close();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }

        void timer_Tick(object sender, EventArgs e)
        {
            lblTime.Content = DateTime.Now.ToLongTimeString();
        }

        //Check internet connection
        private bool CheckConn()
        {
            bool stats = false;
            try
            {
                if (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable() == true)
                {
                    stats = true;
                }
                else
                {
                    stats = false;
                }
                return stats;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
            return stats;
        }

        //Show day1 prop
        private void day1_click(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var s = new DetailedView(forecastManager, forecastManager.Weather.myList[0]);
                s.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBoxResult result = MessageBox.Show(this, "Day 1 Error: " + ex, "Confirmation", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            
        }
        //Show day2 prop
        private void day2_click(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var s = new DetailedView(forecastManager, forecastManager.Weather.myList[0]);
                s.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBoxResult result = MessageBox.Show(this, "Day 2 Error: " + ex, "Confirmation", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

        }
        //Show day3 prop
        private void day3_click(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var s = new DetailedView(forecastManager, forecastManager.Weather.myList[0]);
                s.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBoxResult result = MessageBox.Show(this, "Day 3 Error: " + ex, "Confirmation", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

        }
        //Show day4 prop
        private void day4_click(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var s = new DetailedView(forecastManager, forecastManager.Weather.myList[0]);
                s.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBoxResult result = MessageBox.Show(this, "Day 4 Error: " + ex, "Confirmation", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

        }
        //Show day5 prop
        private void day5_click(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var s = new DetailedView(forecastManager, forecastManager.Weather.myList[0]);
                s.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBoxResult result = MessageBox.Show(this, "Day 5 Error: " + ex, "Confirmation", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

        }


    }
}
